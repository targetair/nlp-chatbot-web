package com.kt.chatbot.nlp.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.chatbot.nlp.dao.TestDao;
import com.kt.chatbot.nlp.vo.ChatbotCategory;

@Service
public class TestServiceImpl implements TestService {
	
	private static final Logger LOG = LoggerFactory.getLogger(TestServiceImpl.class);
	
	@Autowired
	TestDao testDao;

	@Override
	public List<ChatbotCategory> getTestList() {
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.getTestList", getClass());
		}
		
		return testDao.getTestList();
	}

}
