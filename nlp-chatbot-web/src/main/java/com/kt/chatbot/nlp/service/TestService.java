package com.kt.chatbot.nlp.service;

import java.util.List;

import com.kt.chatbot.nlp.vo.ChatbotCategory;

public interface TestService {
	
	public List<ChatbotCategory> getTestList();
	
}
