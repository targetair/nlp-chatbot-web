<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- saved from url=(0039)http://targetai.iptime.org:9080/aiintro -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="/favicon.ico" /> -->
    <title _msthash="149916" _msttexthash="353769">target_air_portal</title>    

	<link rel="stylesheet" href="/nlp/resources/css/common/util.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Common.css" type="text/css" />
	
	<link rel="stylesheet" href="/nlp/resources/css/aiCss.css" type="text/css" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/nlp/resources/js/common/Ajax.js" type="text/javascript"></script>
	
	<script src="/nlp/resources/js/nlp/classification.js" type="text/javascript"></script>

	</head>
		
  <body>
    <noscript>
      <strong
        >We're sorry but target_air_portal doesn't work
        properly without JavaScript enabled. Please enable it to
        continue.</strong
      >
    </noscript>
    <div data-app="true" class="v-application v-application--is-ltr theme--light" id="main-app"><div class="v-application--wrap">
		
		<main class="v-main v-content" data-booted="true" >
<!--NLP시작-->
			<div class="v-main__wrap v-content__wrap" id="Nlp_main-app">
			<div id="ai-intro">
		
		<section id="experience" class="NLP_section">
   <!--LNB시작 -->
			<nav class="Lnb"> 
				<ul>
					<li class="Lnb_1dep">자연어처리(NLP)</li>
					<li class="Lnb_2dep"><a href="/nlp/cbt">챗봇</a></li>
					<li class="Lnb_2dep"><a href="/nlp/mrc">MRC</a></li>
					<li class="Lnb_2dep"><a href="/nlp/qgen">QG</a></li>
					<li class="Lnb_2dep"><a href="/nlp/clfc">Classification</a></li>
			        <li class="Lnb_2dep"><a href="/nlp/nlpfltr">NLP 필터</a></li>			
			    </ul>
				<ul>
					<li class="Lnb_1dep">챗봇빌더</li>
					<li class="Lnb_2dep">카테고리 관리</li>
					<li class="Lnb_2dep">대화시나리오 관리</li>
					<li class="Lnb_2dep">사전 관리</li> 
			    </ul>
				<ul>
					<li class="Lnb_1dep">통계</li>
				</ul>				
			</nav>
   <!--LNB끝 -->
  <!--Contents 시작-->	
			<div class="pa-10 px-16 contents">
				
				<div class="title-left-border my-3">
				<span class="display-1 ml-5 page-sub-title"> Classification </span>
				</div>
				
				<div class="my-3 theme--light" >
					<div  class="text_cont">말 그대로 분류를 뜻하는 Classification은 지도학습의 일종으로 기존에 존재하는 데이터의 Category 관계를 파악하고, 새롭게 관측된 데이터의 Category를 스스로 판별하는 기능입니다.
				    </div>
					
					<div  class="text_info" >
					이 모델은 일상대화의 분류 모델로 Category 구분값은 아래와 같아요. 문장을 입력하여 분류해 보세요~
					</div>
					<div class="category">  
						<ul>
							<li><h4>ex)카테고리 :</h4></li>
						    <li class="category-item">봇 정보</li>
							<li class="category-item">일상표현</li>
							<li class="category-item">긍정적인 표현</li>
							<li class="category-item">부정적인 표현</li>
							<li class="category-item">부탁/명령/제안</li>
							<li class="category-item">사용자 감정</li>						
						</ul>
                    </div>				
				</div>
				<div tabindex="-1" class="v-list-item theme--light"></div>
				
				<h3 class="mb-1"> 문의 내용 </h3>				
			  <div>
				<textarea id="question" rows="5" class="box_01 " style="width:100%">오늘은 정말 기분 좋은 가을 날씨야~</textarea>
				   <div class="text-right mt-4">
					<button id="send-clfc" type="button" class="white--text elevation-2 v-btn v-btn--contained theme--light v-size--default indigo">
						<span class="v-btn__content"> 분류 실행 </span></button>
				  </div>
				  
				  <h3 class="mt-5 mb-1"> 분류 결과 </h3>
				  <div class="v-text-field__slot" > <input id="answer" type="text" readonly="readonly" class="box_03"  style="width:100%" value="사용자 감정"></div>	
				
				</div>			
		    </div>
  <!--Contents 끝-->
				</section>

			</div></div>
<!--NLP끝-->
		</main>

		
		</div>


</body></html>