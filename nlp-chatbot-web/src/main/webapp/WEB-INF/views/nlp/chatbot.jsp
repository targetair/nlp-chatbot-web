<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- saved from url=(0039)http://targetai.iptime.org:9080/aiintro -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="/favicon.ico" /> -->
    <title _msthash="149916" _msttexthash="353769">target_air_portal</title>    

	<link rel="stylesheet" href="/nlp/resources/css/common/util.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Common.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Main.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/chatting.css" type="text/css" />
	
	<link rel="stylesheet" href="/nlp/resources/css/aiCss.css" type="text/css" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/nlp/resources/js/common/Ajax.js" type="text/javascript"></script>
	
	<script src="/nlp/resources/js/nlp/chatbot.js" type="text/javascript"></script>
		

	</head>
	
		
  <body>
    <noscript>
      <strong
        >We're sorry but target_air_portal doesn't work
        properly without JavaScript enabled. Please enable it to
        continue.</strong
      >
    </noscript>
    <div data-app="true" class="v-application v-application--is-ltr theme--light" id="main-app">
    	<div class="v-application--wrap">

		<main class="v-main v-content" data-booted="true" >
<!--NLP시작-->
			<div class="v-main__wrap v-content__wrap" id="Nlp_main-app">
			<div id="ai-intro">
		
		<section id="experience"  class="NLP_section">
   <!--LNB시작 -->
			<nav class="Lnb"> 
				<ul>
					<li class="Lnb_1dep">자연어처리(NLP)</li>
					<li class="Lnb_2dep"><a href="/nlp/cbt">챗봇</a></li>
					<li class="Lnb_2dep"><a href="/nlp/mrc">MRC</a></li>
					<li class="Lnb_2dep"><a href="/nlp/qgen">QG</a></li>
					<li class="Lnb_2dep"><a href="/nlp/clfc">Classification</a></li>
			        <li class="Lnb_2dep"><a href="/nlp/nlpfltr">NLP 필터</a></li>	
			    </ul>
				<ul>
					<li class="Lnb_1dep">챗봇빌더</li>
					<li class="Lnb_2dep">카테고리 관리</li>
					<li class="Lnb_2dep">대화시나리오 관리</li>
					<li class="Lnb_2dep">사전 관리</li> 
			    </ul>
				<ul>
					<li class="Lnb_1dep">통계</li>
				</ul>				
			</nav>
   <!--LNB끝 -->
  <!--Contents 시작-->		
			<div class="pa-10 px-16 wrap_main  txt_al_c">
				<div class="pa-5">
				<div class="my-3">
				    <h1 class="display-1 ml-5 page-sub-title" style="font-family:'KTfontBold'!important; font-size:40px!important;">KTDS 챗봇 </h1>
				</div>				
				<div class="my-3 theme--light" >
					<div  class="chat_title_text" style="font-family:KTfontMedium">KTDS 일상대화 챗봇 입니다. 친구처럼 편안하게 얘기해 보세요. </div>
				</div>
				</div>
				
      <!--Chating 시작-->	
			<div class="cont_chat">				
				<div style="width:600px; height:650px; border:solid 1px #fafafa; border-radius:10px; overflow:hidden;">
				
				
				
					<div class="chatbot_wrap" id="chatbot_wrap">
						<!-- 상단바 -->
						<div class="header_wrap not-a-top">			
							<h1 class="logo" id="logo_txt"><span>테스트 챗봇</span></h1>
						</div>
						<!-- //상단바 -->
			
						<!-- 하단채팅바 -->
						<div class="footer_wrap">
						  <div class="footer write_wrap write_wrap2">			
								<input id="question" type="text" id="text" name="" class="txt_answer" placeholder="챗봇에게 말을 걸어보세요.">
							  
							  <span class="submit_btn"><a href="#" id="send-message"></a></span>
							</div>
						</div>
						<!-- //하단채팅바 -->
				
						<!-- 대화창본문 -->
						<div class="talk_wrap" id="talk_wrap">      
				                      	
					      <div class="chat_area cf">  
					      			<div class="template" style="display:none">                   
					                    <!--질문 메세지-->
								        <div class="message_Q fr">
								            <div class="date"></div>
								            <div class="msg_balloon"></div>
								        </div>
					                     
								         <!--챗봇 답변 -->
								         <div class="message_A loading_A fl">
					                        <div class="profile_img1"></div>
								            <div class="msg_balloon">
					                               
					                        </div>
					                            <div class="date"></div>
								        </div>
					                                     
					                     
							       </div>
				
							</div>
						</div>			    
					</div>
				
				
				</div>	
		    </div>
      <!--Chating 끝-->
			</div>				
  <!--Contents 끝-->
  
		</section>

		</div>
		</div>
<!--NLP끝-->
		</main>

		
		</div>


</body></html>