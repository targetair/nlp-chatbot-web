$(document).ready(function(){
	$("#send-message").click(function(){
		sendMsg();
	});
	
	$('#question').on('keyup', function(e){
		if(e.keyCode != 13){
			return;
		}
		sendMsg();
	});
});

function getTimestamp() {
	var date = new Date();
	var type = (date.getHours() >= 12) ? '오후' : '오전';
	var hours = (date.getHours() >= 13) ? date.getHours() - 12 : (date.getHours() > 0) ? date.getHours() : 12;
	var minutes = date.getMinutes();
	hours = (hours < 10 ? '0' : '') + hours;
	minutes = (minutes < 10 ? '0' : '') + minutes;
	return type + ' ' + hours + ':' + minutes;
}

function sendMsg(){
	let question = $('#question').val().trim();
	
	if(question.length <= 0){
		return;
	}
	
	var data = {
			  question : question
	}
	
	data.userId = "";
	data.botCode = "";
	data.pattern = "";
	data.sessionId = "";
	data.patternPrefix = "";
	
	$('#question').val('');
	
	let smb = $('.template .message_Q').clone();
	smb.find('.msg_balloon').html(question);
	smb.find('.date').html(getTimestamp());
	$('.chat_area').append(smb);
	
	let rmb = $('.template .message_A').clone();
	$('.chat_area').append(rmb);
	
	let scrollGage = $('.chat_area').height() - $('#talk_wrap').height() + 53;
	$('#talk_wrap').scrollTop(scrollGage);
	
	$('#question').attr('readonly', true);
	$('#question').attr('placeholder', '챗봇 응답을 기다리고 있습니다.');
	
	setTimeout(function(){
		Ajax.post({
			  url             : HOSTS.chatbot + '/talk/koSentenceBert'
			, data            : data
			, onSuccess       : function(data){
				
				rmb.removeClass('loading_A');
				rmb.find('.msg_balloon').html(data.answer);
				rmb.find('.date').html(getTimestamp());
				let scrollGage = $('.chat_area').height() - $('#talk_wrap').height() + 53;
				$('#talk_wrap').scrollTop(scrollGage);
			}
			, onError         : function(request, status, error){
				rmb.remove();
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
			, onComplete      : function(){
				$('#question').removeAttr('readonly');
				$('#question').attr('placeholder', '챗봇에게 말을 걸어보세요.');
			}
		});
	}, 300);
	
	
}