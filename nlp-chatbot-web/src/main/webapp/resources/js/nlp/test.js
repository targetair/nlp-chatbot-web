$(document).ready(function(){
	
	let param = {id : 'testUser'}
	
	$('#testSearch').on('click', function(){
		Ajax.post({
			  url             : 'test/list'
			, data            : param
			, onSuccess       : function(r){
				if(r.result){
					$('body').append('<div>'+ JSON.stringify(r.data) +'</div>')
				}else{
					$('body').append('<div>'+ r.message +'</div>')
				}
				
			}
			, onError         : function(request, status, error){
				alert("ERROR");
			}
		});
	});
});