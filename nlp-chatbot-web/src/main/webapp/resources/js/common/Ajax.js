const HOSTS = {api : 'http://targetai.iptime.org:8501', chatbot : 'http://targetai.iptime.org:8502'}

//AJAX 전송하기
function AjaxTransfer(){
    this.isTransferring = false;
}


//POST 전송
AjaxTransfer.prototype.post = function(p){
    var t = this;
    var param = {
		  url  : null
		, type : 'POST'            //기본은 POST
		, dataType: 'json'         //수신 데이터 타입, 기본은 JSON
		, data : null
		, onSuccess : null
		, onError : null
		, onComplete : null
		, contentType : 'application/json; charset=utf-8'
		, async : true
	};

    param = $.extend(param, p);
    
	if(t.isTransferring){
		return;
	}

	t.isTransferring = true;

	param.data = JSON.stringify(param.data);
	
	if(param.url.indexOf(HOSTS.api) >= 0 || param.url.indexOf(HOSTS.chatbot) >= 0){
		delete param.contentType;
	}
	
	param.success = function(data){
		if(param.onSuccess != null){
			param.onSuccess(data);
		}
	};
	param.error = function(request, status, error){
		if(param.onError != null){
			param.onError(request, status, error);
		}
	};
	param.complete = function(data){
		t.isTransferring = false;
		if(param.onComplete != null){
			param.onComplete();
		}
	};

	$.ajax(param);
}

const Ajax = new AjaxTransfer();