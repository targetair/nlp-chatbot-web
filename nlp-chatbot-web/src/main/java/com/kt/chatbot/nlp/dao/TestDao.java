package com.kt.chatbot.nlp.dao;

import java.util.List;

import com.kt.chatbot.nlp.vo.ChatbotCategory;

public interface TestDao {

	public List<ChatbotCategory> getTestList();
	
}
