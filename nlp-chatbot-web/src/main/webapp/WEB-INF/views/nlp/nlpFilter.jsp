<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- saved from url=(0039)http://targetai.iptime.org:9080/aiintro -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="/favicon.ico" /> -->
    <title _msthash="149916" _msttexthash="353769">target_air_portal</title>    

	<link rel="stylesheet" href="/nlp/resources/css/common/util.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Common.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Filter.css" type="text/css" />	
	
	<link rel="stylesheet" href="/nlp/resources/css/aiCss.css" type="text/css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/nlp/resources/js/common/Ajax.js" type="text/javascript"></script>
	
	<script src="/nlp/resources/js/nlp/nlp-filter.js" type="text/javascript"></script>

	</head>
		
  <body>
    <noscript>
      <strong
        >We're sorry but target_air_portal doesn't work
        properly without JavaScript enabled. Please enable it to
        continue.</strong
      >
    </noscript>
    <div data-app="true" class="v-application v-application--is-ltr theme--light" id="main-app"><div class="v-application--wrap">

		<main class="v-main v-content" data-booted="true" >
<!--NLP시작-->		
			<div class="v-main__wrap v-content__wrap" id="Nlp_main-app">
			<div id="ai-intro">
		
		<section id="experience"  class="NLP_section">
   <!--LNB시작 -->
			<nav class="Lnb"> 
				<ul>
					<li class="Lnb_1dep">자연어처리(NLP)</li>
					<li class="Lnb_2dep"><a href="/nlp/cbt">챗봇</a></li>
					<li class="Lnb_2dep"><a href="/nlp/mrc">MRC</a></li>
					<li class="Lnb_2dep"><a href="/nlp/qgen">QG</a></li>
					<li class="Lnb_2dep"><a href="/nlp/clfc">Classification</a></li>
			        <li class="Lnb_2dep"><a href="/nlp/nlpfltr">NLP 필터</a></li>		
			    </ul>
				<ul>
					<li class="Lnb_1dep">챗봇빌더</li>
					<li class="Lnb_2dep">카테고리 관리</li>
					<li class="Lnb_2dep">대화시나리오 관리</li>
					<li class="Lnb_2dep">사전 관리</li> 
			    </ul>
				<ul>
					<li class="Lnb_1dep">통계</li>
				</ul>				
			</nav>
   <!--LNB끝 -->
  <!--Contents 시작-->
			<div class="pa-10 px-16 contents">
				
				<div class="title-left-border my-3">
				<span class="display-1 ml-5 page-sub-title">NLP 필터 </span>
				</div>
				
				<div class="my-3 theme--light" >
					<div  class="text_cont">자연어 처리를 위한 과정중 전처리 및 정확도, 유사도를 높이기 위해서는 다양한 기능이 필요 합니다. 형태소 분석, 맞춤법 검사 등 여러 기능 통해 NLP 전처리 및 성능 튜닝을 테스트 할수있는 페이지 입니다.
				    </div>
					
					<div  class="text_info" >
					입력창 에 문장을 입력하여 필터링 테스트를 해보세요~
					</div>					
					<div tabindex="-1" class="v-list-item theme--light"></div>
				</div>
			
				
			  <div style="height:500px;">
				<div class="struct_01">
				  <div>
					<h3 class="mb-1"> 입력 </h3>
				    <div class="v-text-field__slot" style="display:flex;" > 
					<input id="question" type="text"  class="box_02"  style="width:90%" value="나는 감밥이 겁네 너무 맛있습니다.">
					 <button id="do-process" type="button" class="v-btn v-btn--contained theme--light v-size--default elevation-5 white--text btn_filter" style=""><span class="v-btn__content"> 필터링 </span></button>					  
					  </div>
				  </div>
					
					
				<div style="">
				<h3 class="mb-1">출력 </h3>
				<div class="v-text-field__slot" >
					<textarea id="answer" type="text" readonly="readonly" class="box_03"  style="width:100%" rows="5" >"나,Noun,는,Josa,김밥,Noun,이,Josa,완전,Noun,맛있습니다,Adjective,.,Punctuation"</textarea>
						
						</div>
				</div>
			    </div>
					<div class="struct_02">								
						<img src="/nlp/resources/images/arrow_01.png" width="50" height="50" alt=""/>
				        <img src="/nlp/resources/images/arrow_02.png" width="50" height="50" alt=""/>
				  </div>				
				
				
		        <div class="struct_03">			
				   <h3 class="mt-3 mb-1"> 맞춤법 검사 </h3>
				  <div class="v-text-field__slot"> 
					  <input id="answer_l1" type="text"  class="box_04  box_drag"  style="width:100%" value="나는 김밥이 겁내 너무 맛있습니다." readonly="readonly">
					 </div>
					 <div class="txt_al_c"><img src="/nlp/resources/images/arrow_03.png" alt=""/></div>
					 
				   <h3 class="mt-3 mb-1"> 불용어 처리 </h3>
				  <div class="v-text-field__slot" > 
					  <input id="answer_l2" type="text"  class="box_04  box_drag"  style="width:100%" value="나는 김밥이 너무 맛있습니다." readonly="readonly">					 
					 </div>
					 <div class="txt_al_c"><img src="/nlp/resources/images/arrow_03.png" alt=""/></div>
					 
				   <h3 class="mt-3 mb-1"> 동의어 처리 </h3>
				  <div class="v-text-field__slot" > 
					  <input id="answer_l3" type="text"  class="box_04  box_drag"  style="width:100%" value="나는 김밥이 완전 맛있습니다." readonly="readonly">
					 </div>
					 <div class="txt_al_c"><img src="/nlp/resources/images/arrow_03.png" alt=""/></div>
					 
				   <h3 class="mt-3 mb-1"> 형태소 분석 </h3>
				  <div class="v-text-field__slot"> 
					  <input id="answer_l4" type="text" rows="5"  class="box_04  box_drag" style="width:100%;
						display: -webkit-box;  line-height: 1.2em; height: 3.6em; -webkit-line-clamp: 3;" 
							 value="나,Noun,는,Josa,김밥,Noun,이,Josa,완전,Noun,맛있습니다, Adjective,.,Punctuation" readonly="readonly">	 
					 </div>
				  <div class="text-right mt-4">
					 </div>
				</div>
				
			  </div>			
		    </div>
  <!--Contents 끝-->
				</section>
			</div></div>
		
<!--NLP 끝-->
		</main>
		
		</div>

</body></html>