$(document).ready(function(){
	$("#send-clfc").click(function(){
		sendMsg();
	});
	
	$('#question').on('keyup', function(e){
		if(e.keyCode != 13){
			return;
		}
		sendMsg();
	});
});

function sendMsg(){
	
	let question = $('#question').val();
	
	if(question.length <= 0){
		return;
	}
	
	var data = {
		  question : question
	}
	
	data.userId = "";
	data.botCode = "";
	data.pattern = "";
	data.sessionId = "";
	data.patternPrefix = "";
	

	Ajax.post({
			  url             : HOSTS.api + '/talk/bertClassification'
			, data            : data
			, onSuccess       : function(data){
				$('#answer').val(data.answer);
			}
			, onError         : function(request, status, error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
	});
}
