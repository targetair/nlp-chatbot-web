package com.kt.chatbot.nlp.common.util;

import java.io.IOException;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public class LogbackConfigurer {

	@Value("#{prop['logback.root.level']}")
	private String logLevel;
	
	public void initLogging() throws IOException{
		Logger root = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		Level level = Level.toLevel(logLevel);
		root.setLevel(level);
	}
}
