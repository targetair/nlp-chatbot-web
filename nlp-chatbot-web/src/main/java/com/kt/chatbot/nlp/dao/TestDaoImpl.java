package com.kt.chatbot.nlp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kt.chatbot.nlp.vo.ChatbotCategory;

@Repository
public class TestDaoImpl implements TestDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(TestDaoImpl.class);
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public List<ChatbotCategory> getTestList() {
		
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.getTestList", getClass());
		}


		return sqlSession.selectList("test.selectTestList");
		
	}

}
