package com.kt.chatbot.nlp.vo;

import java.io.Serializable;

public class ChatbotCategory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String chatId;
	private String categoryName;
	private String parent;
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	
}
