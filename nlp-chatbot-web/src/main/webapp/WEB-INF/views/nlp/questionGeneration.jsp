<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- saved from url=(0039)http://targetai.iptime.org:9080/aiintro -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="/favicon.ico" /> -->
    <title _msthash="149916" _msttexthash="353769">target_air_portal</title>    

	<link rel="stylesheet" href="/nlp/resources/css/common/util.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Common.css" type="text/css" />
	
	<link rel="stylesheet" href="/nlp/resources/css/aiCss.css" type="text/css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/nlp/resources/js/common/Ajax.js" type="text/javascript"></script>
	
	<script src="/nlp/resources/js/nlp/question-generation.js" type="text/javascript"></script>

	</head>
		
  <body>
    <noscript>
      <strong
        >We're sorry but target_air_portal doesn't work
        properly without JavaScript enabled. Please enable it to
        continue.</strong
      >
    </noscript>
    <div data-app="true" class="v-application v-application--is-ltr theme--light" id="main-app"><div class="v-application--wrap">

		<main class="v-main v-content" data-booted="true" >
<!--NLP시작-->
			<div class="v-main__wrap v-content__wrap" id="Nlp_main-app">
			<div id="ai-intro">
		
		<section id="experience"  class="NLP_section">
   <!--LNB시작 -->
			<nav class="Lnb"> 
				<ul>
					<li class="Lnb_1dep">자연어처리(NLP)</li>
					<li class="Lnb_2dep"><a href="/nlp/cbt">챗봇</a></li>
					<li class="Lnb_2dep"><a href="/nlp/mrc">MRC</a></li>
					<li class="Lnb_2dep"><a href="/nlp/qgen">QG</a></li>
					<li class="Lnb_2dep"><a href="/nlp/clfc">Classification</a></li>
			        <li class="Lnb_2dep"><a href="/nlp/nlpfltr">NLP 필터</a></li>	
			    </ul>
				<ul>
					<li class="Lnb_1dep">챗봇빌더</li>
					<li class="Lnb_2dep">카테고리 관리</li>
					<li class="Lnb_2dep">대화시나리오 관리</li>
					<li class="Lnb_2dep">사전 관리</li> 
			    </ul>
				<ul>
					<li class="Lnb_1dep">통계</li>
				</ul>				
			</nav>
   <!--LNB끝 -->
  <!--Contents 시작-->			
			<div class="pa-10 px-16 contents">
				
				<div class="title-left-border my-3">
				<span class="display-1 ml-5 page-sub-title"> Question Generation </span>
				</div>
				
				<div class="my-3 theme--light" >
					<div  class="text_cont">질문생성(Question Generation) 는 주어진 원문내 키워드를 제시하면 질문을 생성하는 기술입니다. Pretrained Language Model을 활용하여 주어진 문맥과 정답에 맞춰 Question 을 생성하는 모델로 GPT-2 와 Question Answering Task 에서 좋은 성능을 보여주는 BERT 모델을 조합한 기능을 테스트 할수 있는 페이지 입니다.
				    </div>
					
					<div  class="text_info" >
					원문을 입력하고 원문내 주요키워드를 답변에 입력한 후 QG실행 버튼을 클릭하면 질문을 생성해 줍니다.
					</div>

					<div tabindex="-1" class="v-list-item theme--light"></div>
				</div>
				
				<h3 class="mb-1"> 원문 </h3>
				
			  <div>
				<textarea id="doc" rows="5" class="box_01 " style="width:100%">RAM은 LPDDR4X SDRAM 방식이며 4 GB다. 삼성 엑시노스 9 Series (8895)와 퀄컴 스냅드래곤 835 MSM8998이 LPDDR4X SDRAM 컨트롤러를 내장했다. 또한, 전작인 갤럭시 S7 와 갤럭시 S7 엣지 대비 RAM 용량을 증가시키지 않고 유지하게 되었다. 내장 메모리는 64GB 단일 모델이지만, 삼성 엑시노스 9 Series (8895) 탑재 모델이 UFS 2.1 규격의 낸드 플래시를 사용하고, 퀄컴 스냅드래곤 835 MSM8998 탑재 모델은 UFS 2.0 규격의 낸드 플래시를 사용한다. 공개 당시 삼성전자는 UFS 2.1 규격의 낸드 플래시를 사용한다고 밝혔으나, 이후 확인결과 탑재되는 모바일 AP에 따라 낸드 플래시를 달리 탑재하는 것이 확인되면서 논란이 제기되었다.</textarea>
				  
				  <h3 class="mt-5 mb-1"> 답변 </h3>
				  <div class="v-text-field__slot" > <input id="question" type="text"  class="box_02"  style="width:100%" value="4GB"></div>
				  <div class="text-right mt-4">
					<button id="send-qg"  type="button" class="white--text elevation-2 v-btn v-btn--contained theme--light v-size--default indigo">
						<span class="v-btn__content"> QG실행 </span></button>
				  </div>		  
				  
				  <h3 class="mt-5 mb-1"> 생성된 질문 </h3>
				  <div class="v-text-field__slot" > <input id="answer" type="text" readonly="readonly" class="box_03"  style="width:100%" value="램RAM은 몇GB인가?"></div>			
				</div>			
		    </div>
  <!--Contents 끝-->
				</section>
				
			</div></div>
<!--NLP끝-->
		</main>
		
		</div>

</body></html>