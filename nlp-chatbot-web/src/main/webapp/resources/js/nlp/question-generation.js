$(document).ready(function(){
	$("#send-qg").click(function(){
		sendMsg();
	});
	
	$('#question').on('keyup', function(e){
		if(e.keyCode != 13){
			return;
		}
		sendMsg();
	});
});

function sendMsg(){
	
	let doc = $('#doc').val();
	let question = $('#question').val();
	
	if(doc.trim().length <= 0 || question.length <= 0){
		return;
	}

	var data = {
		  doc      : doc
		, question : question
	}
	

	Ajax.post({
			  url             : HOSTS.api + '/talk/QG'
			, data            : data
			, onSuccess       : function(data){
				$('#answer').val(data.answer);
			}
			, onError         : function(request, status, error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
	});
}
