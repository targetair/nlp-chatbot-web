<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- saved from url=(0039)http://targetai.iptime.org:9080/aiintro -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="/favicon.ico" /> -->
    <title _msthash="149916" _msttexthash="353769">target_air_portal</title>    

	<link rel="stylesheet" href="/nlp/resources/css/common/util.css" type="text/css" />
	<link rel="stylesheet" href="/nlp/resources/css/nlp/nlp_Common.css" type="text/css" />
	
	<link rel="stylesheet" href="/nlp/resources/css/aiCss.css" type="text/css" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/nlp/resources/js/common/Ajax.js" type="text/javascript"></script>
	
	<script src="/nlp/resources/js/nlp/mrc.js" type="text/javascript"></script>
		

	</head>
		
  <body>
    <noscript>
      <strong
        >We're sorry but target_air_portal doesn't work
        properly without JavaScript enabled. Please enable it to
        continue.</strong
      >
    </noscript>
    <div data-app="true" class="v-application v-application--is-ltr theme--light" id="main-app"><div class="v-application--wrap">

		<main class="v-main v-content" data-booted="true" >
			
<!--NLP시작-->
			<div class="v-main__wrap v-content__wrap" id="Nlp_main-app">
			<div id="ai-intro">		
		<section id=""  class="NLP_section">
   <!--LNB시작 -->
			<nav class="Lnb"> 
				<ul>
					<li class="Lnb_1dep">자연어처리(NLP)</li>
					<li class="Lnb_2dep"><a href="/nlp/cbt">챗봇</a></li>
					<li class="Lnb_2dep"><a href="/nlp/mrc">MRC</a></li>
					<li class="Lnb_2dep"><a href="/nlp/qgen">QG</a></li>
					<li class="Lnb_2dep"><a href="/nlp/clfc">Classification</a></li>
			        <li class="Lnb_2dep"><a href="/nlp/nlpfltr">NLP 필터</a></li>	
			    </ul>
				<ul>
					<li class="Lnb_1dep">챗봇빌더</li>
					<li class="Lnb_2dep">카테고리 관리</li>
					<li class="Lnb_2dep">대화시나리오 관리</li>
					<li class="Lnb_2dep">사전 관리</li> 
			    </ul>
				<ul>
					<li class="Lnb_1dep">통계</li>
				</ul>				
			</nav>
  <!--LNB끝  -->
  <!--Contents 시작-->
			<div class="pa-10 px-16 contents">
				
				<div class="title-left-border my-3">
				<span class="display-1 ml-5 page-sub-title"> MRC </span>
				</div>				
				<div class="my-3 theme--light" >
					<div  class="text_cont">기계독해(MRC)는 인공지능(AI) 알고리즘이 스스로 문제를 분석하고 질문에 최적화된 답안을 찾아내는 기술을 말합니다.
사람이 텍스트를 읽고 질문 답변을 추론하듯이 AI가 문장 속에서 의미를 찾고 답변할 수 있습니다. 추론은 불가능하지만 사람이 관련 정보를 찾기 위해 정보를 일일이 확인하지 않아도 답을 찾아낼 수 있는 기능을 테스트 하는 페이지 입니다.
				    </div>					
					<div  class="text_info" >
					원문을 입력하고 원문내 답변을 찾기 위한 질문을 입력한 후 MRC실행 버튼을 클릭하여 최적화된 답을 찾아 보세요. 
					</div>					
					<div tabindex="-1" class="v-list-item theme--light"></div>
				</div>
				
				<h3 class="mb-1"> 원문 </h3>				
			  <div>
				<textarea id="doc" rows="5" class="box_01 " style="width:100%">유재석(劉在錫, 1972년 8월 14일 ~ )은 대한민국의 방송연예인, MC, 희극인이다. 1991년에 한국방송공사(KBS) 공채 개그맨으로 방송 활동을 시작했으며, 《X맨》, 《무한도전》, 《해피투게더》, 《런닝맨》, 《놀면 뭐하니?》, 《유 퀴즈 온 더 블럭》 등 다수의 TV 프로그램 진행을 맡았거나 맡고 있다. 대한민국 지상파 방송 3사(KBS, MBC, SBS)와 백상예술대상의 대상을 총 17회 수상하여 최다 수상 기록을 세웠으며, 2012년 대중문화예술상 국무총리 표창을 받았다. 특유의 재치와 개그로 방송을 재밌게 만들고 후배들도 잘 챙겨주며 여러 방송을 하며 반전 모습과 내면의 모습을 보여줘 많은 사랑
을 받는다.</textarea>
				  
				  <h3 class="mt-5 mb-1"> 질문 </h3>
				  <div class="v-text-field__slot" > <input id="question" type="text"  class="box_02"  style="width:100%" value="백상예술대상 17회에 빛나는 희극인은 누구인가?"></div>
				  <div data-v-a4662e88="" class="text-right mt-4">
					<button  id="send-mrc" type="button" class="white--text elevation-2 v-btn v-btn--contained theme--light v-size--default indigo">
						<span class="v-btn__content"> MRC실행 </span></button>
				  </div>				  
				  
				  <h3 class="mt-5 mb-1"> 생성된 답변 </h3>
				  <div class="v-text-field__slot" > <input id="answer" type="text" readonly="readonly" class="box_03"  style="width:100%" value="유재석"></div>
				</div>			
		    </div>
  <!--Contents 끝-->
				</section>
				
			</div></div>
<!--NLP 끝-->		
		</main>
		
		</div>
		
</body></html>