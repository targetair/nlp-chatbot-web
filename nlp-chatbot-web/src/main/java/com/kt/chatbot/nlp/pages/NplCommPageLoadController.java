package com.kt.chatbot.nlp.pages;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NplCommPageLoadController {
	
	private static final Logger LOG = LoggerFactory.getLogger(NplCommPageLoadController.class);
	
	@RequestMapping(value = "clfc", method = RequestMethod.GET)
	public String classificationView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.classificationView", getClass()); 
		}
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		return "/nlp/classification";
	}
	
	@RequestMapping(value = "mrc", method = RequestMethod.GET)
	public String mrcView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.mrcView", getClass()); 
		}
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		return "/nlp/mrc";
	}
	
	@RequestMapping(value = "nlpfltr", method = RequestMethod.GET)
	public String nlpFilterView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.nlpFilterView", getClass()); 
		}
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		return "/nlp/nlpFilter";
	}
	
	@RequestMapping(value = "qgen", method = RequestMethod.GET)
	public String questionGenerationView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.questionGenerationView", getClass()); 
		}
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		return "/nlp/questionGeneration";
	}
	
	@RequestMapping(value = "cbt", method = RequestMethod.GET)
	public String chatbotView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.chatbotView", getClass()); 
		}
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		return "/nlp/chatbot";
	}
	
}
