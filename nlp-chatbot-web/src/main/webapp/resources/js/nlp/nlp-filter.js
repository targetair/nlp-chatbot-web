$(document).ready(function(){
	$("#do-process").click(function(){
		doSpellCheck();
	});
	
	$('#question').on('keyup', function(e){
		if(e.keyCode != 13){
			return;
		}
		doSpellCheck();
	});
});

async function doSpellCheck(){
	let question = $('#question').val().trim();
	
	if(question.length <= 0){
		return;
	}
	
	var data = {
		question : question
	}
	
	var promise = new Promise((resolve, reject) => {
		Ajax.post({
			  url             : HOSTS.api + '/talk/spellCheck'
			, data            : data
			, onSuccess       : function(data){
				$('#answer_l1').val(data.answer);
				resolve(data.answer);
			}
			, onError         : function(request, status, error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				reject(error);
			}
		});
	});
	
	var answer;
	
	await promise.then(a => {
		answer = a;
	}).catch(error => {
		return;
	});
	
	doStopWordCheck(answer);
};

async function doStopWordCheck(input){
	var data = {
			question : input
	};
	
	var promise = new Promise((resolve, reject) => {
		Ajax.post({
			  url             : HOSTS.api + '/talk/stopword'
			, data            : data
			, onSuccess       : function(data){
				$('#answer_l2').val(data.answer);
				resolve(data.answer);
			}
			, onError         : function(request, status, error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				reject(error);
			}
		});
	});
	
	var answer;
	
	await promise.then(a => {
		answer = a;
	}).catch(error => {
		return;
	});
	
	doSynonymCheck(answer);
}

async function doSynonymCheck(input){
	var data = {
			question : input
	};
	
	var promise = new Promise((resolve, reject) => {
		Ajax.post({
			  url             : HOSTS.api + '/talk/synonym'
			, data            : data
			, onSuccess       : function(data){
				$('#answer_l3').val(data.answer);
				resolve(data.answer);
			}
			, onError         : function(request, status, error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				reject(error);
			}
		});
	});
	
	var answer;
	
	await promise.then(a => {
		answer = a;
	}).catch(error => {
		return;
	});
	
	doMorphemeCheck(answer);
}

function doMorphemeCheck(input){
	var data = {
			question : input
	};
	
	Ajax.post({
		  url             : HOSTS.api + '/talk/twitter'
		, data            : data
		, onSuccess       : function(data){
			$('#answer_l4').val(data.answer);
			$('#answer').val(data.answer);
		}
		, onError         : function(request, status, error){
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

