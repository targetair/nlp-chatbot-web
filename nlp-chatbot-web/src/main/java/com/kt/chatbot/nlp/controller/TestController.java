package com.kt.chatbot.nlp.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.chatbot.nlp.service.TestService;
import com.kt.chatbot.nlp.vo.ChatbotCategory;
import com.kt.chatbot.nlp.vo.ResponseVo;
import com.kt.chatbot.nlp.vo.TestRequestVo;

@Controller
public class TestController {
	private static final Logger LOG = LoggerFactory.getLogger(TestController.class);
	
	@Autowired
	TestService testService;
	
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public String testView(HttpServletResponse response){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.testView", getClass()); 
		}
		
		
		return "/nlp/test";
	}
	
	@RequestMapping(value = "test/list", method = RequestMethod.POST)
	public @ResponseBody ResponseVo getTestList(@RequestBody TestRequestVo testRequest){
		if(LOG.isTraceEnabled()){
			LOG.trace("call {}.getTestList", getClass()); 
		}
		
		ResponseVo responseVo = new ResponseVo();
		
		try {
			LOG.debug("====={}=====", testRequest.getId());

			List<ChatbotCategory> list = testService.getTestList();
			
			responseVo.setData(list);
			
			responseVo.setCode("000");;
			responseVo.setMessage("SUCCESS");
			responseVo.setResult(true);
			
			LOG.debug("====={}=====", list);
			
		}catch(Exception e) {
			LOG.error(e.getMessage(), e);
			responseVo.setCode("001");
			responseVo.setMessage("테스트 오류");
			responseVo.setResult(false);
		}
		
		return responseVo;
	}
}
